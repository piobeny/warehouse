FactoryGirl.define do
  factory :client do
    sequence(:first_name) { |i| "Client nr #{i}"}
    sequence(:last_name) { |i| "Last name nr #{i}"}
    email "MyString"
    address_street "MyString"
    address_city "MyString"
    tel "MyString"
  end
end
