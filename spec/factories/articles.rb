FactoryGirl.define do 
  factory :article do
    sequence(:name) { |i| "Acrticle nr #{i}"}
    sequence(:quantity) { |i| i * 3 }
    sequence(:price) { |i| i * 20 }
  end
end