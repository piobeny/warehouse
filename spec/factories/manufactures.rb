FactoryGirl.define do
  factory :manufacture do
    sequence(:name) { |i| "Manufacture nr #{i}" }
    #association :model
  end
end
