FactoryGirl.define do
  factory :model do
    sequence(:name) { |i| "Model nr#{i}"}
    association :manufacture
    association :category
  end
end