FactoryGirl.define do
  factory :order_article do
    #article FactoryGirl.create(:article)
    #order FactoryGirl.create(:order)
    association :article
    association :order
    sequence(:quantity) { |i| i * 10 }
    sequence(:price) { |i| i * 20 }
  end
end
