FactoryGirl.define do
  factory :category do
    sequence(:name) { |i| "Category nr #{i}"}
    description "Category description"
  end
end
