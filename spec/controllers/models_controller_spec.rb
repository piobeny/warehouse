require 'rails_helper'

RSpec.describe ModelsController, type: :controller do
  context 'json' do
    let(:type) { :model }
    it_behaves_like "shared_json"
  end

  context 'with articles' do
    let(:model) { FactoryGirl.create(:model) }

    before do
      model.articles << FactoryGirl.create(:article)
      sign_in
      get :articles, id: model.id, format: :json
    end

    it 'contains articles' do
      body = JSON.parse(response.body)
      expect(body['models'].first['name']).to eq(model.articles.first.name)
    end
  end
end