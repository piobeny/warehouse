require 'rails_helper'

RSpec.describe ManufacturesController, type: :controller do
  context 'json' do
    let(:type) { :manufacture }
    it_behaves_like "shared_json"
    it_behaves_like "response_with_models"
  end
end
