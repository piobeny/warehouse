require 'rails_helper'

RSpec.describe ClientsController, type: :controller do
  context 'json' do
    let(:type) { :client }
    it_behaves_like "shared_json"
  end
end
