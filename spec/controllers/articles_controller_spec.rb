require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
  context 'json' do
    let(:type) { :article }
    it_behaves_like "shared_json"
  end
end
