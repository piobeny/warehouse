require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  context 'json' do
    let(:type) { :order }
    it_behaves_like "shared_json"
  end

  context "#remove_article" do
    let(:article) { FactoryGirl.create(:article) }
    let(:order) { FactoryGirl.create(:order) }
    before do
      sign_in
      order.add_article(article.id, article.quantity)
      params = {id: order.id, order_article_id: order.order_articles.first.id, format: :json }
      post :remove_article, params      
    end

    it { expect(response).to be_success }

    it { expect(response.body).to eq("") }

    it "should empty articles list" do
      expect(order.articles.count).to eq(0)
    end
  end

  describe "#add_article" do
    let(:article) { FactoryGirl.create(:article) }
    let(:order) { FactoryGirl.create(:order) }

    context "when user authenticated" do
      before { sign_in }

      before do 
        params = {id: order.id, article_id: article.id, quantity: article.quantity, price: article.price, format: :json }
        post :add_article, params
      end

      it "returns order_article with given article in response" do
        body = JSON.parse(response.body)['order_article']
        expect(body['article_id']).to eq(article.id)
      end

      it { expect(response).to be_success }
      
      it 'adds an article' do
        expect(order.reload.order_articles.count).to eq(1)
      end
    end

    context "when user is signed out" do
      let(:params) {{id: order.id, article_id: article.id, quantity: article.quantity, price: article.price, format: :json }}
      it_behaves_like "protected_action", :post, :add_article
    end
  end

end
