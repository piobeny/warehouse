require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  context 'json' do
    let(:type) { :category }
    it_behaves_like "shared_json"
    it_behaves_like "response_with_models"
  end
end
