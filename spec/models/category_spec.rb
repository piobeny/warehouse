require 'rails_helper'

RSpec.describe Category, type: :model do
  context 'without name' do
    subject(:category) { Category.new }

    it 'raise error' do
      expect { category.save! }.to raise_error(
        ActiveRecord::RecordInvalid
        )
    end
    it 'is not valid' do
      expect(category).to validate_presence_of(:name)
    end
  end

  context 'with models' do
    subject(:category) { FactoryGirl.create(:category) }
    before { category.models = [model1, model2] }
    let(:model1) { FactoryGirl.create(:model) }
    let(:model2) { FactoryGirl.create(:model) }

    it 'have two models' do
      expect(category.models.count).to eq 2
    end

    it 'include this models' do
      expect(category.models).to include(model1, model2)
    end
  end

  it { expect { FactoryGirl.build(:category).save! }.to change { Category.count }.by(1) }
end
