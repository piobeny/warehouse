require 'rails_helper'

RSpec.describe Order, type: :model do 
  context 'with article' do
    subject (:order) { FactoryGirl.create(:order) }
    let(:article) { FactoryGirl.create(:article) }

    it 'add order_article changes orders summary price' do
      expect { order.add_article(article.id, 1) }.to change { order.summary_price }.by(article.price)
    end

    before { order.add_article(article, 1) }
    it 'check summary price works' do
      expect(order.summary_price).to eq(article.price)
    end

    it 'has article' do
      expect(order.articles.count).to eq(1)
    end

    it 'removes article' do
      order_article = order.order_articles.first
      order.remove_article(order_article.id)
      expect(order.articles.count).to eq(0)
    end

    it 'connot add more than quantity' do
      expect(order.add_article(article.id, article.quantity * 2)).to eq(false)
    end
  end

  context 'cannot destroy' do
    subject (:order) { FactoryGirl.create(:order) }
    before { order.destroy }

    it 'archivized should be true' do
      expect(order.archivized).to eq(true)
    end
    it 'there should be order' do
      expect(Order.count).to eq(1)
    end
  end
end
