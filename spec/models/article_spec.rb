require 'rails_helper'

RSpec.describe Article, type: :model do
  context 'without article' do
    subject(:article) { FactoryGirl.build(:article) }
    context "a name" do
      before { article.name = nil }
      it 'raise error' do
        expect { article.save! }.to raise_error(
          ActiveRecord::RecordInvalid
          )
      end

      it 'is invalid' do
        expect(article).to validate_presence_of(:name)
      end
    end

    context "a quantity" do
      before { article.quantity = nil }

      it 'raise error' do
        expect { article.save! }.to raise_error(
          ActiveRecord::RecordInvalid
          )
      end

      it 'is invalid' do
        expect(article).to validate_presence_of(:quantity)
      end
    end
  end

  it 'created 100 articles' do
    x = 100
    expect { x.times { FactoryGirl.create(:article) } }.to change { Article.count }.by(x)
  end

  context 'with article' do
    subject(:article) { FactoryGirl.create(:article) }
    let(:order) { FactoryGirl.create(:order) }

    it 'add article to order changes article quantity' do
      expect { order.add_article(article.id, 1) }.to change { Article.find(article.id).quantity }.by(-1)
    end

    it 'has buyer' do
      order.add_article(article.id, 1)
      expect(article.buyers).not_to be_empty
    end
  end
end
