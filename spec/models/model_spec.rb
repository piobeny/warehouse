require 'rails_helper'

RSpec.describe Model, type: :model do
  it 'is invalid without a name' do
    expect(subject).to validate_presence_of(:name)
  end
  context 'with' do
    subject(:model) { Model.new(name: "Model with articles")}
    context 'articles' do
      before { model.articles = [art1, art2]}
      let(:art1) { Article.new(name: "Green glasses", fabric: "plastic", quantity: 10, price: 100) }
      let(:art2) { Article.new(name: "Red glasses", fabric: "metal", quantity: 5, price: 200) }
      it 'include articles' do
        expect(model.articles).to include(art1)
        expect(model.articles).to include(art2)
      end

      it 'amout of models change' do
        expect { model.save! }.to change { Model.count }.by(1)
      end
    end
  end

  it 'created 100 models' do
    expect { 100.times { FactoryGirl.create(:model) } }.to change { Model.count }.by(100)
  end

  it 'raise error without name' do
    expect { subject.save! }.to raise_error(
      ActiveRecord::RecordInvalid
      )
  end
end
