require 'rails_helper'

RSpec.describe Manufacture, type: :model do
  context 'without name' do
    subject(:manufacture) { Manufacture.new }
    it 'raise error' do
      expect { manufacture.save! }.to raise_error(
          ActiveRecord::RecordInvalid
        )
    end

    it 'be invalid' do
      expect(manufacture).to validate_presence_of(:name)
    end
  end

  context 'with models' do
    subject(:manufacture) { FactoryGirl.create(:manufacture)}
    before { manufacture.models = [model1, model2] }
    let(:model1) { FactoryGirl.create(:model) }
    let(:model2) { FactoryGirl.create(:model) }
    
    it 'should include' do
      expect(manufacture.models).to include(model1)
      expect(manufacture.models).to include(model2)
    end

    it 'should have two models' do
      expect(manufacture.models.count).to eq 2
    end
  end

  it { expect { FactoryGirl.build(:manufacture).save! }.to change { Manufacture.count }.by(1) }

end
