require 'rails_helper'

RSpec.describe OrderArticle, type: :model do
  it 'is invalid without a quantity' do
    expect(subject).to validate_presence_of(:quantity)
  end

  #xit 'cannot add more articles then there is (in article.quantity)'

end
