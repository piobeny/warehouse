require 'rails_helper'

shared_examples_for 'protected_action' do |method, action|
  before do
    if defined? params
      send(method, action, params)
    else
      send(method, action)
    end
  end

  it  { expect(response.status).to eq 401 }
end