require 'rails_helper'

shared_examples_for 'response_with_models' do 
  let(:shared) { FactoryGirl.create(type) }

  before do
    sign_in
    shared.models << FactoryGirl.create(:model)
    get :show, id: shared.id, format: :json
  end

  it 'in body' do
    body = JSON.parse(response.body)
    model = shared.models.first
    expect(body['models'].first['name']).to eq(model.name) 
  end
  
  it { expect(response).to be_success }
end