require 'rails_helper'

shared_examples_for "shared_json" do
  let(:shared) { FactoryGirl.create(type) }
  describe 'user signed id' do
    before { sign_in }
    context 'index' do
      before do
        get :index, format: :json 
      end

      it "responds with JSON" do
        expect(response.header['Content-Type']).to include('application/json')
      end

      it 'is success' do
        expect(response).to be_success
      end

    end

    context 'show' do
      before do
        shared.save!
        get :show, id: shared.id, format: :json 
      end

      it 'response with status 200' do
        expect(response).to be_success
      end

      it 'responds with JSON example' do
        body = JSON.parse(response.body)[type.to_s]
        expect(body['name']).to eq(shared.name) if shared.respond_to?(:name)
        expect(body['id']).to eq(shared.id)
      end
    end
  end

  context 'user signed out' do
    it_behaves_like "protected_action", :get, :index
    
    let(:params) { {id: shared.id, format: :json} }
    it_behaves_like "protected_action", :get, :show
  end
end
