class CategoriesController < ApiController
  before_action :set_category, only: [:show, :models, :manufactures, :articles]

  def index
    categories = Category.all

    respond_to do |format|
      format.json { render json: categories }
    end
  end


  def show
    respond_to do |format|
      format.json { render json: @category }
    end
  end

  #get :models
  def models
    models = @category.models

    respond_to do |format|
      format.json { render json: models }
    end
  end

  #get :manufactures
  def manufactures
    manufactures = @category.manufactures

    respond_to do |format|
      format.json { render json: manufactures }
    end
  end

  #get :articles
  def articles
    articles = @category.articles

    respond_to do |format|
      format.json { render json: articles }
    end
  end

  private
  def set_category
    @category = Category.find(params[:id])
  end
end
