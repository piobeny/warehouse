class OrdersController < ApiController
  before_action :set_order, only: [:show, :articles, :add_article, :remove_article]
  #get :index
  def index
    orders = Model.all

    respond_to do |format|
      format.json { render json: orders }
    end
  end
  
  #get :show
  def show
    respond_to do |format|
      format.json { render json: @order }
    end
  end

  #get :articles
  def articles
    respond_to do |format|
      format.json { render json: @order.articles }
    end
  end

  def create
    #client_id
    order = Order.new(art_params)
    respond_to do |format|
      if order.save
        format.json { render json: order }
      else
        format.json { render json: order.errors, status: :unprocessable_entity }
      end
    end
  end

  #post :add_article
  def add_article
    if @order.add_article(art_params[:article_id], art_params[:quantity], art_params[:price])
      #head :no_content
      respond_to do |format|
        format.json { render json: @order.order_articles.last }
      end
    end
  end

  def remove_article
    if @order.remove_article(art_params[:order_article_id])
      head :no_content
    end
  end

  private
  def set_order
    @order = Order.find(params[:id])
  end

  def art_params
    params.permit(:id, :article_id, :quantity, :price, :order_article_id, :client_id)
  end
end
