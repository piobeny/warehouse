class ArticlesController < ApiController
  before_action :set_article, only: [:show, :clients]

  def index
    articles = Article.all

    respond_to do |format|
      format.json { render json: articles }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @article }
    end
  end

  def clients
    respond_to do |format|
      format.json { render json: @article.buyers }
    end
  end

  private
  def set_article
    @article = Article.find(params[:id])
  end
end
