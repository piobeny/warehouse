class ModelsController < ApiController
    before_action :set_model, only: [:show, :articles]

    #get :index
    def index
      models = Model.all

      respond_to do |format|
        format.json { render json: models }
      end
    end
    
    #get :show
    def show
      respond_to do |format|
        format.json { render json: @model }
      end
    end

    #get :articles
    def articles
      articles = @model.articles

      respond_to do |format|
        format.json { render json: articles }
      end
    end


    private
    def set_model
      @model = Model.find(params[:id])
    end
end
