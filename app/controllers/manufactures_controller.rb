class ManufacturesController < ApiController
  before_action :set_manufacture, only: [:show, :models, :articles, :categories]
  # GET /monufactures
  # GET /monufactures.json
  def index
    manufactures = Manufacture.all

    respond_to do |format|
      format.json { render json: manufactures }
    end
  end

  # GET /monufactures/1
  # GET /monufactures/1.json
  def show
    respond_to do |format|
      format.json { render json: @manufacture }
    end
  end

  def models
    models = @manufacture.models

    respond_to do |format|
      format.json { render json: models }
    end
  end

  #articles
  def articles
    articles = @manufacture.articles

    respond_to do |format|
      format.json { render json: articles }
    end
  end

  #categories
  def categories
    categories = @manufacture.categories
    
    respond_to do |format|
      format.json { render json: categories }
    end
  end

  private
  def set_manufacture
    @manufacture = Manufacture.find(params[:id])
  end
end
