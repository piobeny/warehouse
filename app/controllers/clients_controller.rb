class ClientsController < ApiController
  before_action :set_client, only: [:show, :orders]

  def index
    clients = Client.all

    respond_to do |format|
      format.json { render json: clients }
    end
  end

  def show
    respond_to do |format|
      format.json { render json: @client }
    end
  end

  def orders
    respond_to do |format|
      format.json { render json: @client.orders }
    end
  end

  private
  def set_client
    @client = Client.find(params[:id])
  end
end
