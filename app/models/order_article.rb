class OrderArticle < ActiveRecord::Base
  belongs_to :article
  belongs_to :order
  validates :article_id, presence: true
  validates :order_id, presence: true
  validates :quantity, presence: true
end
