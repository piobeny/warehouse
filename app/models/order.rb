class Order < ActiveRecord::Base
  belongs_to :client
  has_many :order_articles
  has_many :articles, through: :order_articles
  scope :archivized, -> { where(archivized: true) }
  scope :current, -> { where(archivized: false) }
  #default_scope { where(archivized: false) }


  before_destroy :archive

  def summary_price
    OrderArticle.where(order: self).inject(0){ |sum, x| sum + (x.price * x.quantity) }
  end

  def add_article(article_id, quantity, price=nil)
    article = Article.find(article_id)
    quantity = quantity.to_f
    return false if article.quantity < quantity
    article.quantity -= quantity
    article.save!
    OrderArticle.create(article: article, order: self, quantity: quantity, price: price ||= article.price )
    true
  end

  def remove_article(order_article_id)
    order_article = OrderArticle.find(order_article_id)
    a = order_article.article
    a.quantity += order_article.quantity
    a.save!
    order_article.destroy
  end

  private 
  def archive
    self.archivized = true
    save!
    false
  end
end
