class Article < ActiveRecord::Base
  belongs_to :model
  has_many :order_articles
  has_many :orders, through: :order_articles

  validates :name, presence: true 
  validates :quantity, presence: true
  validates :price, presence: true

  scope :available, -> { where("quantity > 0") }

  def buyers
    ids = []
    self.orders.joins(:client).select("clients.id").distinct.each { |x| ids << x[:id]}
    Client.find(ids)
  end
end
