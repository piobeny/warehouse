class Category < ActiveRecord::Base
  has_many :models
  has_many :articles, through: :models
  has_many :manufactures, -> { distinct }, through: :models
  validates :name, presence: true 
end
