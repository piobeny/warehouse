class Model < ActiveRecord::Base
  validates :name, presence: true
  has_many :articles
  belongs_to :manufacture
  belongs_to :category
end
