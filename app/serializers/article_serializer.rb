class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :name, :fabric, :quantity, :price

end
