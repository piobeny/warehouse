class ClientSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :email, :address_street, :address_city, :tel, :orders_count

  def orders_count
    object.orders.count
  end
end
