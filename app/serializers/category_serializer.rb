class CategorySerializer < ActiveModel::Serializer
  attributes :id, :name, :description

  has_many :models, embed: :ids, include: true
end
