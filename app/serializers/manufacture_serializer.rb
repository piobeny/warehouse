class ManufactureSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :models, embed: :ids, include: true
end
