class OrderArticleSerializer < ActiveModel::Serializer
  attributes :id, :article_id, :quantity, :price, :article_name

  def article_name
    object.article.name
  end
end
