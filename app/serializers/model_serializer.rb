class ModelSerializer < ActiveModel::Serializer
  attributes :id, :name, :manufacture_id, :category_id, :manufacture_name, :category_name, :articles_count

  #has_one :manufacture
  #has_one :category
  def manufacture_name
    object.manufacture.name
  end
  def category_name
    object.category.name
  end
  def articles_count
    object.articles.count
  end
end
