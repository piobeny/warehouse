class OrderSerializer < ActiveModel::Serializer
  attributes :id, :client_name, :created_at, :summary_price

  has_many :order_articles, embed: :ids, include: true

  def client_name
    "#{object.client.first_name} #{object.client.last_name}"
  end

  def summary_price
    object.summary_price
  end
end
