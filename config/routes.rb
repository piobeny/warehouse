Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  mount_devise_token_auth_for 'User', at: 'auth'
  
  with_options only: [:show, :index] do |list|
    list.resources :manufactures do
      member do
        #show models, categories or articles in which products from manufature are
        get :models
        get :categories
        get :articles
      end
    end

    list.resources :categories do
      member do
        #show models/articles or manufacture in this category
        get :models
        get :manufactures
        get :articles
      end
    end

    list.resources :models do
      member do
        get :articles   
      end 
    end

    list.resources :articles do
      member do
        get :clients
      end
    end

    list.resources :clients do
      member do
        #show orders made by this client
        get :orders
      end
    end
  end

  resources :orders do
    member do
      #show articles on specific order
      get :articles
      post :add_article
      post :remove_article
    end
  end
end
