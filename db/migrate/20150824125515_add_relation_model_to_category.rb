class AddRelationModelToCategory < ActiveRecord::Migration
  def change
    add_column :models, :category_id, :string
    add_index :models, :category_id
  end
end
