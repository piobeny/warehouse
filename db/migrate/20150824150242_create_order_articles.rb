class CreateOrderArticles < ActiveRecord::Migration
  def change
    create_table :order_articles do |t|
      t.references :article, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
