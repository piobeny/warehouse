class AddArchivizedToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :archivized, :boolean, :default => false
  end
end
