class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :address_street
      t.string :address_city
      t.string :tel

      t.timestamps null: false
    end
  end
end
