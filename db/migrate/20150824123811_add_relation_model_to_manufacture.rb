class AddRelationModelToManufacture < ActiveRecord::Migration
  def change
    add_column :models, :manufacture_id, :string
    add_index :models, :manufacture_id
  end
end
