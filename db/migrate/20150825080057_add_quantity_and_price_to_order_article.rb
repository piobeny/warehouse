class AddQuantityAndPriceToOrderArticle < ActiveRecord::Migration
  def change
    add_column :order_articles, :quantity, :integer
    add_column :order_articles, :price, :decimal, precision: 8, scale: 2
    add_column :articles, :price, :decimal, precision: 8, scale: 2
  end
end
