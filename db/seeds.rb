# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

#10.times do |x|
#  Manufacture.create(name: "Manufacture nr #{x}")
#  Category.create(name: "Category nr #{x}")
#end

5.times do |x|
  Model.create(name: "Model nr #{x}", manufacture: Manufacture.first, category: Category.first)
  Model.create(name: "Model nr #{x + 5}", manufacture: Manufacture.find(2), category: Category.find(2))
end

5.times do |x|
  Article.create(name: "Article nr #{x}", fabric: "plastic", quantity: x * 10, model: Model.first, price: 100)
  Article.create(name: "Article nr #{x + 5}", fabric: "metal", quantity: x * 5, model: Model.find(2), price: 200)
end